import { useEffect, useRef, useState } from 'react'
import { Button, Space } from 'antd'
import { getStudentGroupApi } from '@/services/class'
import TablePro, { type TableInstance } from '@/components/TablePro/TablePro'
import SearchForm from '@/components/SearchForm/SearchForm'
import Permission from '@/components/Permission/Permission'
import { filterList } from './constants/filterList'
import { getColumns } from './constants/columns'


const ClassList = () => {
  const tableRef = useRef<TableInstance>(null)
  const [params, setParams] = useState<any>({})

  const columns = getColumns({
    actions: (_, record) => (
      <Space size="middle">
        <Button size="small">编辑</Button>
        <Permission permissionKey="delUserBtn">
          <Button size="small" danger>删除</Button>
        </Permission>
      </Space>
    )
  })

  useEffect(() => {
    if (params) {
      tableRef.current?.getData()
    }
  }, [params])

  return (
    <div>
      <SearchForm
        filterList={filterList}
        onSearch={(value) => {
          setParams(value)
        }}
        onReset={(value) => {
          setParams(value)
        }}
      />
      <TablePro
        ref={tableRef}
        columns={columns}
        request={async ({ page, pagesize }) => {
          const res = await getStudentGroupApi({ page, pagesize, ...params })
          return res.data
        }}
        scroll={{
          x: 1300
        }}
        headerTitle="班级列表"
        toolBarRender={() => <Button type="primary">创建班级</Button>}
      />
    </div>
  )
}

export default ClassList
