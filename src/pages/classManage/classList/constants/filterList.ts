import type { FilterListItem } from '@/components/SearchForm/SearchForm'
import { FilterType } from '@/components/SearchForm/constant'



export const filterList: FilterListItem[] = [
  {
    label: '班级名称',
    name: 'name',
    type: FilterType.input,
    placeholder: '请输入班级名称'
  },
  {
    label: '班级科目',
    name: 'classify',
    type: FilterType.select,
    placeholder: '请选择科目',
    props: {
      allowClear: true,
      options: [
        {
          value: '男',
          label: '男',
        },
        {
          value: '女',
          label: '女'
        },
      ]
    }
  },
  {
    label: '班级教师',
    name: 'teacher',
    type: FilterType.select,
    placeholder: '请选择教师',
    props: {
      allowClear: true,
      options: [
        {
          value: '男',
          label: '男',
        },
        {
          value: '女',
          label: '女'
        },
      ]
    }
  }
]