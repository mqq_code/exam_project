import type { TableProps } from 'antd'
import type { ClassListItem } from '@/types/api/class'
import moment from 'moment'


interface ColumnsRender {
  actions: (_: ClassListItem, record: ClassListItem) => React.ReactNode
}
export const getColumns = ({ actions }: ColumnsRender) => {
  const columns: TableProps<ClassListItem>['columns']  = [
    {
      title: '班级名称',
      dataIndex: 'name',
      key: 'name',
      fixed: 'left'
    },
    {
      title: '班级科目',
      dataIndex: 'classify',
      key: 'classify',
    },
    {
      title: '班级教师',
      dataIndex: 'teacher',
      key: 'teacher',
    },
    {
      title: '学生人数',
      dataIndex: 'students',
      key: 'students',
      render: (_, record) => (record.students.length)
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      render: (_, record) => (
        <span>{moment(record.createTime).format('YYYY-MM-DD HH:mm:ss')}</span>
      )
    },
    {
      title: '创建人',
      dataIndex: 'creator',
      key: 'creator',
    },
    {
      title: '操作',
      key: 'action',
      fixed: 'right',
      width: 240,
      render: actions
    }
  ]

  return columns
}
