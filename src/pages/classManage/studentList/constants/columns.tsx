import type { TableProps } from 'antd'
import type { Student } from '@/types/api/class'
import moment from 'moment'


interface ColumnsRender {
  actions: (_: Student, record: Student) => React.ReactNode
}
export const getColumns = ({ actions }: ColumnsRender) => {
  const columns: TableProps<Student>['columns']  = [
    {
      title: '姓名',
      dataIndex: 'username',
      key: 'name',
      fixed: 'left'
    },
    {
      title: '班级',
      dataIndex: 'className',
      key: 'className',
    },
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
    },
    {
      title: '年龄',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      render: (_, record) => (
        <span>{moment(record.createTime).format('YYYY-MM-DD HH:mm:ss')}</span>
      )
    },
    {
      title: '创建人',
      dataIndex: 'creator',
      key: 'creator',
    },
    {
      title: '操作',
      key: 'action',
      fixed: 'right',
      width: 240,
      render: actions
    }
  ]

  return columns
}
