import { useEffect, useRef, useState } from 'react'
import { Button, Space } from 'antd'
import { getStudentListApi } from '@/services/class'
import TablePro, { type TableInstance } from '@/components/TablePro/TablePro'
import { getColumns } from './constants/columns'


const StudentList = () => {
  const tableRef = useRef<TableInstance>(null)

  const columns = getColumns({
    actions: (_, record) => (
      <Space size="middle">
        <Button size="small">编辑</Button>
      </Space>
    )
  })

  return (
    <div>
      <TablePro
        ref={tableRef}
        columns={columns}
        request={async ({ page, pagesize }) => {
          const res = await getStudentListApi({ page, pagesize })
          return res.data
        }}
        scroll={{
          x: 1300
        }}
        headerTitle="学生列表"
        toolBarRender={() => <Button type="primary">添加学生</Button>}
      />
    </div>
  )
}

export default StudentList
