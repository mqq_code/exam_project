import { useEffect, useRef, useState } from 'react'
import { Button, Space,  } from 'antd'
import style from './userList.module.scss'
import { getUserlistApi, roleListApi } from '@/services/systemApi'
import TablePro, { type TableInstance } from '@/components/TablePro/TablePro'
import SearchForm from '@/components/SearchForm/SearchForm'
import Permission from '@/components/Permission/Permission'
import SetRole from './components/SetRole'
import { filterList } from './constants/filterList'
import { getColumns } from './constants/columns'
import type { UserListParams } from '@/types/api/systemApi'
import { useRequest } from 'ahooks'

type FilterUserList = Omit<UserListParams, 'page' | 'pagesize'> & {
  startTime?: number,
  endTime?: number
}

const UserList = () => {
  const tableRef = useRef<TableInstance>(null)
  const [params, setParams] = useState<FilterUserList>({} as FilterUserList)
  const { data } = useRequest(roleListApi)

  const columns = getColumns({
    actions: (_, record) => (
      <Space size="middle">
        <SetRole
          id={record._id}
          role={record.role || []}
          roleList={data?.data.list || []}
          onSuccess={() => {
            tableRef.current?.getData()
          }}
        />
        <Button size="small">编辑</Button>
        <Permission permissionKey="delUserBtn">
          <Button size="small" danger>删除</Button>
        </Permission>
      </Space>
    )
  })

  useEffect(() => {
    if (params) {
      tableRef.current?.getData()
    }
  }, [params])

  return (
    <div className={style.page}>
      <SearchForm
        filterList={filterList}
        onSearch={({ lastOnlineTime, ...value }) => {
          const timeParams: FilterUserList = {}
          if (lastOnlineTime) {
            timeParams.startTime = +lastOnlineTime[0]
            timeParams.endTime = +lastOnlineTime[1]
          }
          setParams({
            ...value,
            ...timeParams
          })
        }}
        onReset={(value) => {
          setParams(value)
        }}
      />
      <TablePro
        ref={tableRef}
        columns={columns}
        request={async ({ page, pagesize }) => {
          const res = await getUserlistApi({ page, pagesize, ...params })
          return res.data
        }}
        scroll={{
          x: 1300
        }}
        headerTitle="用户列表"
        toolBarRender={() => <Button type="primary">添加用户</Button>}
      />
    </div>
  )
}

export default UserList
