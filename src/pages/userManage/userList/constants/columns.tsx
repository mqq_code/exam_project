import type { TableProps } from 'antd'
import type { UserListItem } from '@/types/api/systemApi'
import moment from 'moment'
import { Switch, Image, Tag } from 'antd'

interface ColumnsRender {
  actions: (_: UserListItem, record: UserListItem) => React.ReactNode
}
export const getColumns = ({ actions }: ColumnsRender) => {
  const columns: TableProps<UserListItem>['columns']  = [
    {
      title: '用户名',
      dataIndex: 'username',
      key: 'username',
      fixed: 'left',
      width: 100
    },
    {
      title: '密码',
      dataIndex: 'password',
      key: 'password',
    },
    {
      title: '账号状态',
      dataIndex: 'status',
      key: 'status',
      render: (_, record) => (
        <Switch
          disabled={record.username === 'root'}
          defaultChecked={record.status == 1}
          onChange={async () => {}}
        />
      )
    },
    {
      title: '头像',
      dataIndex: 'avator',
      key: 'avator',
      render: (_, record) => record.avator ? <Image width={100} src={record.avator} /> : '未设置'
    },
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
      render: (_, record) => record.sex ? <Tag>{record.sex}</Tag> : '未设置'
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email',
      render: (_, record) => record.email || '--'
    },
    {
      title: '最近登录',
      dataIndex: 'lastOnlineTime',
      key: 'lastOnlineTime',
      width: 200,
      render: (_, record) => (record.lastOnlineTime ? moment(record.lastOnlineTime).format('YYYY-MM-DD kk:mm:ss') : '--')
    },
    {
      title: '创建人',
      dataIndex: 'creator',
      key: 'creator',
    },
    {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 240,
      render: actions
    }
  ]

  return columns
}
