import type { FilterListItem } from '@/components/SearchForm/SearchForm'
import { FilterType } from '@/components/SearchForm/constant'



export const filterList: FilterListItem[] = [
  {
    label: '姓名',
    name: 'username',
    type: FilterType.input,
    placeholder: '请输入姓名'
  },
  {
    label: '账号状态',
    name: 'status',
    type: FilterType.select,
    placeholder: '请输入账号状态',
    props: {
      allowClear: true,
      options: [
        {
          value: 1,
          label: '启用',
        },
        {
          value: 0,
          label: '禁用'
        },
      ],
    }
  },
  {
    label: '性别',
    name: 'sex',
    type: FilterType.select,
    placeholder: '请选择性别',
    props: {
      allowClear: true,
      options: [
        {
          value: '男',
          label: '男',
        },
        {
          value: '女',
          label: '女'
        },
      ]
    }
  },
  {
    label: '邮箱',
    name: 'email',
    type: FilterType.input,
    placeholder: '请输入邮箱'
  },
  {
    label: '最近登录',
    name: 'lastOnlineTime',
    type: FilterType.date,
    placeholder: '请输入最近登录',
    props: {
      showTime: true
    }
  }
]