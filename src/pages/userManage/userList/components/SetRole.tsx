import { Form, Select, message } from 'antd'
import ModalButton from '@/components/ModalButton/ModalButton'
import { editUserApi } from '@/services/systemApi'
import { useEffect } from 'react'
import type { RoleListItem } from '@/types/api/systemApi'
interface Props {
  id: string;
  role: string[];
  roleList: RoleListItem[];
  onSuccess: () => void;
}

const SetRole = ({
  role,
  id,
  roleList,
  onSuccess
}: Props) => {
  const [form] = Form.useForm()

  useEffect(() => {
    form?.setFieldValue('role', role)
  }, [role, form])

  return (
    <ModalButton
      buttonText="分配角色"
      modalTitle="分配角色"
      buttonProps={{
        size: 'small'
      }}
      onOk={async () => {
        const value = await form.validateFields()
        const res = await editUserApi({...value, id})
        if (res.code === 200) {
          message.success('添加成功')
          onSuccess()
        } else {
          message.error(res.msg)
          return false
        }
      }}
    >
      <Form form={form}>
        <Form.Item
          name="role"
          rules={[
            {
              required: true,
              message: '请选择角色',
            }
          ]}
        >
          <Select
            placeholder="请选择用户角色"
            mode="multiple"
            fieldNames={{
              label: 'name',
              value: 'value'
            }}
            options={roleList}
          />
        </Form.Item>
      </Form>
    </ModalButton>
  )
}

export default SetRole