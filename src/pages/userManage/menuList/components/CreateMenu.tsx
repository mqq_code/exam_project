import { Form, Input, Select, Switch, message } from 'antd'
import ModalButton from '@/components/ModalButton/ModalButton'
import { createMenuApi } from '@/services/systemApi'
import { useMemo } from 'react'
import type { PermissionListItem } from '@/types/api/systemApi'

interface Props {
  menuList: PermissionListItem[];
  onSuccess?: () => void;
}

const CreateMenu: React.FC<Props> = ({
  menuList,
  onSuccess
}) => {
  const [form] = Form.useForm()
  const options = useMemo(() => {
    const base = [
      {
        label: '创建一级菜单',
        value: 'first'
      }
    ]
    return base.concat(menuList.map(v => ({
      label: v.name,
      value: v._id
    })))
  }, [menuList])

  return (
    <ModalButton
      buttonText="添加权限"
      modalTitle="添加权限"
      onOk={async () => {
        const value = await form.validateFields()
        const res = await createMenuApi({
          ...value,
          disabled: !value.disabled
        })
        if (res.code === 200) {
          message.success('添加成功')
          if (onSuccess) {
            onSuccess()
          }
        } else {
          message.error(res.msg)
          return false
        }
      }}
    >
      <Form
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 18,
        }}
        form={form}
        initialValues={{
          disabled: true,
        }}
      >
        <Form.Item
          label="权限层级"
          name="pid"
          rules={[
            {
              required: true,
              message: '请选择权限层级!',
            },
          ]}
        >
          <Select placeholder="请选择" options={options} />
        </Form.Item>
        <Form.Item
          label="权限名称"
          name="name"
          rules={[
            {
              required: true,
              message: '请输入权限名称',
            },
          ]}
        >
          <Input placeholder="请输入名称" />
        </Form.Item>
        <Form.Item
          label="权限路径"
          name="path"
          rules={[
            {
              required: true,
              message: '请输入权限路径',
            },
          ]}
        >
          <Input placeholder="请输入路径" />
        </Form.Item>
        <Form.Item
          label="权限类型"
          name="isBtn"
          rules={[
            {
              required: true,
              message: '请选择权限类型!',
            },
          ]}
        >
          <Select
            placeholder="请选择"
            options={[
              {
                label: '页面',
                value: false
              },
              {
                label: '按钮',
                value: true
              }
            ]}
          />
        </Form.Item>
        <Form.Item
          label="权限状态"
          name="disabled"
        >
          <Switch checkedChildren="启用" unCheckedChildren="禁用"></Switch>
        </Form.Item>
      </Form>
    </ModalButton>
  )
}

export default CreateMenu