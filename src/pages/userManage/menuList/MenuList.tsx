import { useRef, useState } from 'react'
import TablePro, { type TableInstance } from '@/components/TablePro/TablePro'
import { getMenuListApi, removeMenuApi, editMenuApi } from '@/services/systemApi'
import { Space, Button, Switch, Tag, Popconfirm, message } from 'antd'
import moment from 'moment'
import CreateMenu from './components/CreateMenu.jsx'
import Permission from '@/components/Permission/Permission'
import type { TableProps } from 'antd'
import type { PermissionListItem } from '@/types/api/systemApi'
const MenuList = () => {

  const tableRef = useRef<TableInstance>(null)
  const columns: TableProps<PermissionListItem>['columns'] = [
    {
      title: '权限名称',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: '权限路径',
      dataIndex: 'path',
      key: 'path',
    },
    {
      title: '权限类型',
      dataIndex: 'isBtn',
      key: 'isBtn',
      render: isBtn => (
        isBtn ? <Tag color='#87d068'>按钮权限</Tag> : <Tag color='#108ee9'>页面权限</Tag>
      )
    },
    {
      title: '是否启用',
      dataIndex: 'disabled',
      key: 'disabled',
      render: (disabled, record) => (
        <Switch
          defaultChecked={!disabled}
          checkedChildren="启用"
          unCheckedChildren="禁用"
          onChange={async (flag) => {
            const res = await editMenuApi({
              id: record._id,
              disabled: !flag
            })
            if (res.code === 200) {
              message.success('修改成功！')
            } else {
              message.error(res.msg)
            }
          }}
        />
      )
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      width: 200,
      render: _ => (_ ? moment(_).format('YYYY-MM-DD kk:mm:ss') : '--')
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Button size="small" type="primary">编辑</Button>
          <Popconfirm
            key="del"
            title="确认删除?"
            onConfirm={async () => {
              const res = await removeMenuApi(record._id)
              if (res.code === 200) {
                message.success('删除成功')
                tableRef.current?.getData()
              } else {
                message.error(res.msg)
              }
            }}
            okText="确认"
            cancelText="取消"
          >
            <Permission permissionKey="delMenuBtn">
              <Button size="small" danger>删除</Button>
            </Permission>
          </Popconfirm>
        </Space>
      ),
    },
  ]
  const [list, setList] = useState<PermissionListItem[]>([])
  return (
    <div>
      <TablePro
        ref={tableRef}
        headerTitle="菜单列表"
        toolBarRender={() => (
          <CreateMenu
            menuList={list}
            onSuccess={() => {
              tableRef.current?.getData()
            }}
          />
        )}
        columns={columns}
        request={async () => {
          const res = await getMenuListApi()
          setList(res.data.list)
          return res.data
        }}
        pagination={false}
      />
    </div>
  )
}

export default MenuList