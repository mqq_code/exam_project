import { Form, Input, Switch, message } from 'antd'
import ModalButton from '@/components/ModalButton/ModalButton'
import { createRoleApi } from '@/services/systemApi'

interface Props {
  onSuccess: () => void
}
const CreateRole: React.FC<Props> = ({
  onSuccess
}) => {
  const [form] = Form.useForm()
 
  return (
    <ModalButton
      buttonText="创建角色"
      modalTitle="创建角色"
      onOk={async () => {
        const value = await form.validateFields()
        const res = await createRoleApi({
          ...value,
          disabled: !value.disabled
        })
        if (res.code === 200) {
          message.success('添加成功')
          onSuccess()
        } else {
          message.error(res.msg)
          return false
        }
      }}
    >
      <Form
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 18,
        }}
        form={form}
        initialValues={{
          disabled: true,
        }}
      >
        <Form.Item
          label="角色名称"
          name="name"
          rules={[
            {
              required: true,
              message: '请输入角色名称',
            },
          ]}
        >
          <Input placeholder="请输入角色名称" />
        </Form.Item>
        <Form.Item
          label="角色关键字"
          name="value"
          rules={[
            {
              required: true,
              message: '请输入角色关键字',
            },
          ]}
        >
          <Input placeholder="请输入角色关键字" />
        </Form.Item>
        <Form.Item
          label="是否启用"
          name="disabled"
        >
          <Switch checkedChildren="启用" unCheckedChildren="禁用"></Switch>
        </Form.Item>
      </Form>
    </ModalButton>
  )
}

export default CreateRole