import TablePro, { type TableInstance } from '@/components/TablePro/TablePro'
import { roleListApi, removeRoleApi, editRoleApi, getMenuListApi } from '@/services/systemApi'
import { Space, Button, Switch, Popconfirm, message, Drawer, Tree } from 'antd'
import moment from 'moment'
import CreateRole from './components/CreateRole.jsx'
import { useRef, useState } from 'react'
import { useRequest } from 'ahooks'
import Permission from '@/components/Permission/Permission'
import type { TableProps, TreeProps } from 'antd'
import type { RoleListItem } from '@/types/api/systemApi'

type CheckedKeys = {
  checked: React.Key[];
  halfChecked: React.Key[];
}
const RoleList = () => {
  const columns: TableProps<RoleListItem>['columns'] = [
    {
      title: '角色民称',
      dataIndex: 'name',
      key: 'name',
      fixed: 'left',
      width: 100
    },
    {
      title: '角色关键字',
      dataIndex: 'value',
      key: 'value',
    },
    {
      title: '创建人',
      dataIndex: 'creator',
      key: 'creator',
    },
    {
      title: '是否启用',
      dataIndex: 'disabled',
      key: 'disabled',
      render: (disabled, record) => (
        <Switch
          defaultChecked={!disabled}
          onChange={async (flag) => {
            const res = await editRoleApi({
              id: record._id,
              disabled: !flag
            })
            if (res.code === 200) {
              message.success('修改成功！')
            } else {
              message.error(res.msg)
            }
          }}
        />
      )
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      width: 200,
      render: _ => (_ ? moment(_).format('YYYY-MM-DD kk:mm:ss') : '--')
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Button size="small" type="primary" onClick={() => showPermission(record)}>分配权限</Button>
          <Popconfirm
            key="del"
            title="确认删除?"
            onConfirm={async () => {
              const res = await removeRoleApi(record._id)
              if (res.code === 200) {
                message.success('删除成功')
                tableRef.current?.getData()
              } else {
                message.error(res.msg)
              }
            }}
            okText="确认"
            cancelText="取消"
          >
            <Permission permissionKey="delRoleBtn">
              <Button size="small" danger>删除</Button>
            </Permission>
          </Popconfirm>
        </Space>
      ),
    },
  ]
  const tableRef = useRef<TableInstance>(null)
  // 编辑角色弹窗
  const [editId, setEdit] = useState('')
  // 选中的权限
  const [defaultRree, setDefaultRree] = useState<CheckedKeys>({
    checked: [],
    halfChecked: []
  })
  const [loading, setLoading] = useState(false)
  
  // 角色列表
  const { data: treeData } = useRequest(getMenuListApi)


  // 展示权限列表
  const showPermission = (record: RoleListItem) => {
    setEdit(record._id)
    // 反显选中的tree
    const checkedKeys: CheckedKeys = {
      checked: [], // 选中的菜单
      halfChecked: [] // 子元素部分选中的菜单
    }
    treeData?.data.list.forEach((item) => {
      // 查找此项菜单是否存在当前角色中
      if (record.permission.includes(item._id)) {
        // 判断此项菜单的所有子菜单是否都在当前角色中
        if (item.children?.every((v) => record.permission.includes(v._id))) {
          checkedKeys.checked.push(item._id, ...item.children.map((v) => v._id))
        } else {
          // 如果子菜单不是都在当前角色中就把此项添加到 halfChecked 
          if (item.children) {
            checkedKeys.halfChecked.push(item._id)
            item.children.forEach((v) => {
              // 把选中的子菜单添加到checked
              if (record.permission.includes(v._id)) {
                checkedKeys.checked.push(v._id)
              }
            })
          } else {
            checkedKeys.checked.push(item._id)
          }
        }
      }
    })
    setDefaultRree(checkedKeys)
  }

  // 选中菜单
  const onCheck: TreeProps['onCheck'] = (checkedKeys, info) => {
    console.log(checkedKeys, info)
    setDefaultRree({
      checked: checkedKeys as React.Key[],
      halfChecked: info.halfCheckedKeys || []
    })
  }
  // 提交角色
  const confirm = async () => {
    setLoading(true)
    const res = await editRoleApi({
      id: editId,
      permission: [...defaultRree.checked, ...defaultRree.halfChecked] as string[]
    })
    setLoading(false)
    if (res.code === 200) {
      tableRef.current?.getData()
      setEdit('')
      message.success('修改成功')
    } else {
      message.error(res.msg)
    }
  }
  return (
    <>
      <TablePro
        ref={tableRef}
        headerTitle="角色列表"
        toolBarRender={() =>
          <CreateRole
            onSuccess={() => {
              tableRef.current?.getData()
            }}
          />
        }
        columns={columns}
        request={async () => {
          const res = await roleListApi()
          return res.data
        }}
        pagination={false}
      />
      <Drawer
        title="分配角色"
        placement="right"
        destroyOnClose
        open={!!editId}
        onClose={() => setEdit('')}
        footer={
          <Button type="primary" onClick={confirm} loading={loading}>
            确定
          </Button>
        }
      >
        <Tree
          fieldNames={{ title: 'name', key: '_id', children: 'children'}}
          defaultExpandAll
          checkable
          checkedKeys={defaultRree}
          selectable={false}
          onCheck={onCheck}
          treeData={treeData?.data.list || []}
          disabled={loading}
        />
      </Drawer>
    </>
  )
}

export default RoleList