import { useRequest } from 'ahooks'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Row, Col, Form, Input, Spin, message } from 'antd'
import { useNavigate } from 'react-router-dom'
import style from './login.module.scss'
import {
  loginApi,
  getCaptchaApi
} from '@/services/user'


const Login = () => {
  const navigate = useNavigate()
  const { run: refreshCaptcha, data: captchaData, loading: capLoading  } = useRequest(getCaptchaApi)
  const { run: runLogin, loading } = useRequest(loginApi, {
    manual: true,
    onSuccess: (res) => {
      if (res.code === 200) {
        message.success('登录成功!')
        localStorage.setItem('exam_token', res.data.token)
        navigate('/home')
      } else if (res.code === 1005) {
        message.error(res.msg)
        refreshCaptcha()
      } else {
        message.error(res.msg)
      }
    }
  })

  return (
    <div className={style.login}>
      <h2>考试管理后台</h2>
      <Form
        className={style.loginForm}
        onFinish={runLogin}
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: '请输入用户名!',
            },
          ]}
        >
          <Input size="large" prefix={<UserOutlined/>} placeholder="用户名" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: '请输入密码!',
            },
          ]}
        >
          <Input.Password size="large" prefix={<LockOutlined/>} placeholder="密码" />
        </Form.Item>
        <Form.Item
          name="code"
          rules={[
            {
              required: true,
              message: '请输入验证码!',
            },
          ]}
        >
          <Row gutter={20}>
            <Col span={14}>
              <Input size="large" prefix={<LockOutlined />} placeholder="请输入验证码" />
            </Col>
            <Col span={10}>
              <div className={style.code} onClick={refreshCaptcha}>
                <Spin spinning={capLoading}>
                  <img src={captchaData?.data.code} alt="" />
                </Spin>
              </div>
            </Col>
          </Row>
        </Form.Item>
        <Form.Item>
          <Button loading={loading} type="primary" size="large" htmlType="submit" block>登录</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Login