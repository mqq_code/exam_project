import { Tabs } from 'antd'
import SingleQuestion from './components/SingleQuestion/SingleQuestion'
import MultipleQuestion from './components/MultipleQuestion/MultipleQuestion'


const CreateQuestion = () => {
  const items = [
    {
      key: '1',
      label: '创建试题',
      children: <SingleQuestion />,
    },
    {
      key: '2',
      label: '批量导入',
      children: <MultipleQuestion />
    }
  ]
  return (
    <div>
      <Tabs defaultActiveKey="1" items={items} />
    </div>
  )
}

export default CreateQuestion