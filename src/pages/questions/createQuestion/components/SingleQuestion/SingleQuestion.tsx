import React, { useMemo } from 'react'
import { Form, Input, Select, Button, Row, Col, Space, Radio, Checkbox, message } from 'antd'
import { getClassifyListApi } from '../../../../../services/class'
import { useRequest } from 'ahooks'

const SingleQuestion = () => {
  const [form] = Form.useForm()
  const typeValue = Form.useWatch('type', form)
  const { data: classifyData } = useRequest(getClassifyListApi)

  const classifyOptions = useMemo(() => {
    return classifyData?.data.list || []
  }, [classifyData])

  const onFinish = (value: any) => {
    console.log(value)
  }
  const renderAnswer = () => {
    if (typeValue === 1) {
      return (
        <Form.Item
          label="选项"
          name="answer"
          rules={[
            {
              required: true,
              message: '请选择正确答案!',
            }
          ]}
        >
          <Radio.Group>
            <Radio value="A">item 1</Radio>
            <Radio value="B">item 2</Radio>
            <Radio value="C">item 3</Radio>
            <Radio value="D">item 4</Radio>
          </Radio.Group>
        </Form.Item>
      )
    } else if (typeValue === 2) {
      return (
        <Form.Item
          label="选项"
          name="answer"
          rules={[
            {
              required: true,
              message: '请选择正确答案!',
            }
          ]}
        >
          <Checkbox.Group>
            <Checkbox value="A">A</Checkbox>
            <Checkbox value="B">B</Checkbox>
            <Checkbox value="C">C</Checkbox>
            <Checkbox value="D">D</Checkbox>
          </Checkbox.Group>
        </Form.Item>
      )
    } else if (typeValue === 3) {
      return (
        <Form.Item
          label="选项"
          name="answer"
          rules={[
            {
              required: true,
              message: '请选择正确答案!',
            }
          ]}
        >
          <Radio.Group>
            <Radio value="A">item 1</Radio>
            <Radio value="B">item 2</Radio>
          </Radio.Group>
        </Form.Item>
      )
    } else if (typeValue === 4) {
      return (
        <Form.Item
          label="选项"
          name="answer"
          rules={[
            {
              required: true,
              message: '请输入答案!',
            }
          ]}
        >
          <Input.TextArea placeholder="输入填空题答案" />
        </Form.Item>
      )
    }
  }
  
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={onFinish}
    >
      <Form.Item
        label="题目"
        name="question"
        rules={[
          {
            required: true,
            message: '请输入题目!',
          },
        ]}
      >
        <Input.TextArea placeholder="输入题目"/>
      </Form.Item>
      <Row gutter={20}>
        <Col span={12}>
          <Form.Item
            label="题型"
            name="type"
            rules={[
              {
                required: true,
                message: '请选择题型',
              },
            ]}
          >
            <Select
              placeholder="请选择题型"
              options={[
                {
                  label: '单选',
                  value: 1
                },
                {
                  label: '多选',
                  value: 2
                },
                {
                  label: '判断',
                  value: 3
                },
                {
                  label: '填空',
                  value: 4
                }
              ]}
            />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            label="科目"
            name="classify"
            rules={[
              {
                required: true,
                message: '请选择科目',
              },
            ]}
          >
            <Select
              placeholder="请选择科目"
              options={classifyOptions}
              fieldNames={{
                label: 'name'
              }}
            />
          </Form.Item>
        </Col>
      </Row>
      {typeValue && renderAnswer()}
      <Form.Item
        label="解析"
        name="desc"
      >
        <Input.TextArea placeholder="输入题目解析"/>
      </Form.Item>
      <Form.Item>
        <Space>
          <Button type="primary" htmlType="submit">提交</Button>
          <Button>重置</Button>
        </Space>
      </Form.Item>
    </Form>
  )
}

export default SingleQuestion