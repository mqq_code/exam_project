import { Button, Result } from 'antd'
import { Link } from 'react-router-dom'
const Notfound = () => {
  return (
    <Result
      status="403"
      title="403"
      subTitle="当前用户没有此页面访问权限，请向管理员申请权限"
      extra={<Button type="primary"><Link to="/home">去首页</Link></Button>}
    />
  )
}

export default Notfound