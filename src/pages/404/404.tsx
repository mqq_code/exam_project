import { Button, Result } from 'antd'
import { Link } from 'react-router-dom'
const Notfound = () => {
  return (
    <Result
      status="404"
      title="404"
      subTitle="此页面不存在"
      extra={<Button type="primary"><Link to="/home">去首页</Link></Button>}
    />
  )
}

export default Notfound