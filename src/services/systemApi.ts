import { get, post } from '@/utils/request'
import type {
  ResponseBase,
  UserList,
  UserListParams,
  UserCreateParams,
  Permission,
  CreateRoleParams,
  RoleList,
  PermissionParams
} from '@/types/api/systemApi' 

// 获取用户列表
export const getUserlistApi = (params: UserListParams) => {
  return get<ResponseBase<UserList>>('/user/list', {
    params,
  })
}

// 创建用户
export const newUser = (params: UserCreateParams) => {
  return post<ResponseBase>('/user/create', {
    params
  })
}

// 编辑用户
export const editUserApi = (params: Partial<UserCreateParams>) => {
  return post<ResponseBase>('/user/update', params)
}

// 删除用户
export const removeUserApi = (id: string) => {
  return post<ResponseBase>('/user/remove', {
    id
  })
}

// 创建菜单
export const createMenuApi = (data: PermissionParams) => {
  return post<ResponseBase>('/permission/create', data)
}

// 查询菜单
export const getMenuListApi = () => {
  return get<ResponseBase<Permission>>('/permission/list')
}

// 编辑菜单
export const editMenuApi = (data: Partial<PermissionParams> & { id: string }) => {
  return post<ResponseBase>('/permission/update', data)
}

// 删除菜单
export const removeMenuApi = (id: string) => {
  return post<ResponseBase>('/permission/remove', {
    id
  })
}

// 创建角色
export const createRoleApi = (params: CreateRoleParams) => {
  return post<ResponseBase>('/role/create', params)
}

// 查询角色接口
export const roleListApi = async () => {
  return get<ResponseBase<RoleList>>('/role/list')
}

// 删除角色
export const removeRoleApi =  (id: string) => {
  return post<ResponseBase>('/role/remove', { id })
}

// 编辑角色
export const editRoleApi = (params: Partial<CreateRoleParams> & { id: string }) => {
  return post<ResponseBase>('/role/update', params)
}
