import { get, post } from '@/utils/request'
import { ResponseBase } from '@/types/api/base'
import {
  LoginParams,
  LoginResponse,
  Captcha,
  UserInfo,
  LeftMenuList
} from '@/types/api/user'


// 登录
export const loginApi = (params: LoginParams) => {
  return post<ResponseBase<LoginResponse>>('/login', params)
}

// 发送验证码
export const getCaptchaApi = async () => {
  return get<ResponseBase<Captcha>>('/login/captcha')
}

// 个人信息接口
export const userInfoApi = async () => {
  return get<ResponseBase<UserInfo>>('/user/info')
}

// 查询左侧菜单
export const menulistApi = async () => {
  return get<ResponseBase<LeftMenuList>>('/user/menulist')
}

// 编辑个人信息
// export const editUserInfo = (params) => {
//   return post('/user/update/info', params)
// }