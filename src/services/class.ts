import { get, post } from '@/utils/request'
import type {
  ResponseBase,
  ClassListRes,
  StudentListRes,
  ClassifyRes
} from '@/types/api/class'

// 班级列表
export const getStudentGroupApi = (params: any) => {
  return get<ResponseBase<ClassListRes>>('/studentGroup/list', {
    params,
  })
}

// 新增班级
export const createStudentGroupApi = (data: any) => {
  return post('/studentGroup/create', {
    data,
  })
}
// 删除班级
export const delStudentGroupApi = (id: string) => {
  return post('/studentGroup/remove', {
    data: {
      id,
    },
  })
}
// 获取科目列表
export const getClassifyListApi = (params = {}) => {
  return get<ResponseBase<ClassifyRes>>('/classify/list', {
    params,
  })
}

// 学生列表
export const getStudentListApi = (params: any) => {
  return get<ResponseBase<StudentListRes>>('/student/list', {
    params,
  })
}
// 新增学生
export const createStudentApi = (data: any) => {
  return post('/student/create', {
    data,
  })
}
// 删除学生
export const delStudentApi = (id: string) => {
  return post('/student/remove', {
    data: {
      id,
    },
  })
}
