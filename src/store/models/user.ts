import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { userInfoApi, menulistApi } from '@/services/user'
import type { RootState } from '../index'
import { UserInfo, LeftMenuItem } from '@/types/api/user'

// 请求用户信息接口
export const getUserInfo = createAsyncThunk('user/getUserInfo', async () => {
  const res = await userInfoApi()
  return res
})
// 请求左侧菜单接口
export const getMenulist = createAsyncThunk('user/getMenulist', async () => {
  const res = await menulistApi()
  return res
})

interface UserState {
  loading: boolean;
  userInfo: UserInfo | null;
  menuList: LeftMenuItem[];
}
const initialState: UserState = {
  loading: true,
  userInfo: null, // 用户信息
  menuList: [] // 左侧菜单
}

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
  },
  extraReducers: builder => {
    builder
      .addCase(getUserInfo.pending, (state) => {
        state.loading = true
      })
      .addCase(getUserInfo.fulfilled, (state, { payload }) => {
        // 用户信息请求成功存数据
        state.loading = false
        state.userInfo = payload.data
      })
      .addCase(getMenulist.fulfilled, (state, { payload }) => {
        state.menuList = payload.data.list
      })
  }
})

export const selectMenu = (state: RootState) => {
  return state.user.menuList
}


export default userSlice.reducer