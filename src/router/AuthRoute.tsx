import { Suspense, useEffect } from 'react'
import PageLoading from '../components/PageLoading/PageLoading'
import PageHeader from '../components/PageHeader/PageHeader'
import { useAppSelector } from '@/hooks/store'
import { useLocation, useNavigate } from 'react-router-dom'
import type { RouteItem } from './routes'

const AuthRoute = ({ route }: { route: RouteItem }) => {
  // 获取当前用户权限列表
  const permission = useAppSelector(state => state.user.userInfo?.permission || [])
  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    // 如果当前路由需要校验登录状态
    if (route.auth) {
      // 从权限列表中查找当前路由是否存在，不存在就跳转到 403
      const currentRoute = permission.find(v => v.path === location.pathname)
      if (!currentRoute) {
        navigate('/403')
      }
    }
  }, [location.pathname, permission])

  if (route.title) {
    document.title = route.title
  }
  // 判断传入的是异步组件还是可以直接使用的组件
  const RouteElement = route.component ? <route.component /> : route.element
  return <Suspense fallback={<PageLoading />}>
    {route.pageHeader ? 
      <PageHeader>
        <div style={{ margin: 20, background: '#ffffff', padding: 20 }}>
          {RouteElement}
        </div>
      </PageHeader>
      :
      RouteElement
    }
  </Suspense>
}

export default AuthRoute