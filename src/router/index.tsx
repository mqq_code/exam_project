import routes,  { type RouteItem } from './routes'
import AuthRoute from './AuthRoute'
import { RouteObject } from 'react-router-dom'

// 遍历路由表，给每一项路由添加校验的外层组件
const routesMap = (routesConfig: RouteItem[]): RouteObject[]  => {
  return routesConfig.map((item: RouteItem) => {
    // 有children就添加，没有就添加
    const children = item.children ? { children: routesMap(item.children) } : {}
    return {
      path: item.path,
      // 给所有的路由组件添加拦截，在拦截组件内渲染路由
      element: <AuthRoute route={item} />,
      ...children
    }
  })
}
const router = routesMap(routes)

export default router