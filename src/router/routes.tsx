import { Navigate } from 'react-router-dom'
import { lazy } from 'react'
import Layout from '../layout/Layout'
import Notfound from '../pages/404/404'
import Home from '../pages/home/Home'
import React from 'react'

export interface RouteItem {
  path: string;
  element?: JSX.Element;
  auth?: boolean;
  title?: string;
  pageHeader?: boolean;
  children?: RouteItem[];
  component?: React.ComponentType;
}

// 配置路由，配置页面标题，是否需要拦截
const routes: RouteItem[] = [
  {
    path: '/',
    element: <Layout />,
    children: [
      {
        path: '/',
        element: <Navigate to="/home" />
      },
      {
        path: '/home',
        element: <Home />,
        title: '首页',
      },
      {
        path: '/userManage/manage-page',
        // 异步组件通过 component 属性传递，为了和 element 元素做区分
        component: lazy(() => import('../pages/userManage/userList/UserList')),
        title: '用户管理',
        auth: true, // 需要登录鉴权
        pageHeader: true
      },
      {
        path: '/userManage/system',
        component: lazy(() => import('../pages/userManage/roleList/RoleList')),
        title: '角色管理',
        auth: true,
        pageHeader: true
      },
      {
        path: '/userManage/menuManage',
        component: lazy(() => import('../pages/userManage/menuList/MenuList')),
        title: '菜单管理',
        auth: true,
        pageHeader: true
      },
      {
        path: '/userManage/personal',
        component: lazy(() => import('../pages/userManage/userInfo/UserInfo')),
        title: '个人信息',
        auth: true,
        pageHeader: true
      },
      {
        path: '/userManage/personal',
        component: lazy(() => import('../pages/userManage/userInfo/UserInfo')),
        title: '个人信息',
        auth: true,
        pageHeader: true
      },
      {
        path: '/manage-group/group-list',
        component: lazy(() => import('../pages/classManage/classList/ClassList')),
        title: '班级列表',
        auth: true,
        pageHeader: true
      },
      {
        path: '/manage-group/group-students',
        component: lazy(() => import('../pages/classManage/studentList/StudentList')),
        title: '学生列表',
        auth: true,
        pageHeader: true
      },
      {
        path: '/question/item-bank',
        component: lazy(() => import('../pages/questions/questionList/QuestionList')),
        title: '试题列表',
        auth: true,
        pageHeader: true
      },
      {
        path: '/question/create-item',
        component: lazy(() => import('../pages/questions/createQuestion/CreateQuestion')),
        title: '创建试题',
        auth: true,
        pageHeader: true
      },
      {
        path: '/question/create-subject',
        component: lazy(() => import('../pages/questions/createSubject/CreateSubject')),
        title: '科目管理',
        auth: true,
        pageHeader: true
      }
    ]
  },
  {
    path: '/login',
    component: lazy(() => import('../pages/login/Login')),
    title: '登录'
  },
  {
    path: '/403',
    component: lazy(() => import('../pages/403/403')),
    title: '403'
  },
  {
    path: '*',
    element: <Notfound />,
    title: '404'
  }
]

export default routes