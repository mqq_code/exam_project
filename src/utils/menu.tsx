import { Link } from 'react-router-dom'
import { LeftMenuItem } from '@/types/api/user'

interface MenuItem  {
  key: string;
  title: string;
  label: JSX.Element | string;
  children?: MenuItem[]
}

export const baseMenu: MenuItem[] = [
  {
    key: '/home',
    title: '监控页面',
    label: <Link to="/home">监控页面</Link>
  }
]

// 格式化菜单数据
export const formatMenu = (list: LeftMenuItem[]) => {
  return list.map(v => {
    const item: MenuItem = {
      key: v.path,
      label: v.children ? v.name : <Link to={v.path}>{v.name}</Link>,
      title: v.name
    }
    if (v.children) {
      item.children = formatMenu(v.children)
    }
    return item
  })
}

// 根据 key 查找所有父级菜单
export const getParentList = (list: MenuItem[], key: string, parent: MenuItem[] = []): MenuItem[] => {
  for (const val of list) {
    if (val.key === key) {
      return [...parent, val]
    }
    if (val.children) {
      const childRes = getParentList(val.children, key, [...parent, val])
      if (childRes.length > 0) {
        return childRes
      }
    }
  }
  return []
}