import axios, { AxiosRequestConfig } from 'axios'
import { message } from 'antd'

// const BaseURL = 'https://zyxcl.xyz/exam_api'
const BaseURL = '/api'


const request = axios.create({
  baseURL: BaseURL,
  timeout: 30000
})

// 请求拦截器
request.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('exam_token')
    if (token) {
      config.headers.authorization = token
    }
    return config
  },
  (error) => {
    return Promise.reject(new Error(error))
  }
)

request.interceptors.response.use(
  (res) => {
    return res.data
  },
  (error) => {
    if (error.response.status === 401) {
      message.error('登录信息失效，请重新登录！')
      location.assign('#/login')
      return Promise.reject(new Error('登录信息失效'))
    } else if (error.response.status === 403) {
      message.error('没有权限访问')
      location.assign('#/403')
      return Promise.reject(new Error('没有权限'))
    }
    message.error('错误码' + error.response.status)
    return Promise.reject(new Error(error))
  }
)

export const get = <T>(url: string, config?: AxiosRequestConfig): Promise<T> => request.get(url, config)
export const post = <T, D = unknown>(url: string, data?: D, config?: AxiosRequestConfig): Promise<T> => request.post(url, data, config)

export default request