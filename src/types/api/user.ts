// 登陆参数
export type LoginParams = {
  username: string;
  password: string;
  code: string;
}
// 登陆返回值
export type LoginResponse = {
  token: string;
}
// 验证码
export type Captcha = {
  code: string;
}
// 权限菜单
export interface IPermission {
  createTime: number;
  disabled: boolean;
  isBtn: boolean;
  name: string;
  path: string;
  pid: string;
  __v: number;
  _id: string;
}
// 用户信息
export type UserInfo = {
  _id: string;
  username: string;
  password: string;
  status: 0 | 1;
  role: string | string[];
  sex: string;
  age: string;
  email: string;
  avator?: string;
  permission: IPermission[];
}

// 查询左侧菜单列表
export interface LeftMenuItem {
  createTime: number;
  disabled: boolean;
  isBtn: boolean;
  name: string;
  path: string;
  __v: number;
  _id: string;
  children?: LeftMenuItem[];
  icon?: any;
}
export interface LeftMenuList {
  list: LeftMenuItem[];
}