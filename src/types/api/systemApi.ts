export type { ResponseBase } from './base'

// 查询用户列表
export type UserListParams = {
  page: number;
  pagesize: number;
  username?: string;
  role?: string;
  status?: 0 | 1;
  sex?: '男' | '女';
  email?: string;
  lastOnlineTime?: number;
}
export type UserListItem = {
  name(arg0: string, name: any): unknown;
  id?: string;
  disabled?: boolean | undefined;
  username: string;
  creator?: string;
  password?: string;
  role?: string[];
  lastOnlineTime?: number;
  status: 0 | 1;
  sex?: '男' | '女';
  email?: string;
  _id: string;
  avator?: string;
}
export type UserList = {
  list: UserListItem[];
  total: number;
  totalPage: number;
}

// 新增用户
export type UserCreateParams = {
  username: string;
  password: string;
  role: string[];
  status: 0 | 1;
  sex: '男' | '女';
  email: string;
}

// 权限列表
export type PermissionParams = {
  name: string;
  path: string;
  pid: string;
  disabled: boolean;
  isBtn: boolean;
}
export type PermissionListItem = {
  createTime: number;
  disabled: boolean;
  isBtn: boolean;
  name: string;
  path: string;
  pid: string;
  _id: string;
  children?: PermissionListItem[];
}
export type Permission = {
  list: PermissionListItem[];
}

// 角色列表
export type CreateRoleParams = {
  name: string;
  value: string;
  disabled: boolean;
  permission?: string[];
}
export type RoleListItem = {
  _id: string;
  name: string;
  value: string;
  creator: string;
  createTime: number;
  disabled: boolean;
  permission: string[];
}
export type RoleList = {
  list: RoleListItem[];
  total: number;
  totalPage: number;
}