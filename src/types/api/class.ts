export type { ResponseBase } from './base'

export type Student = {
  exams: string[];
  _id: string;
  username: string;
  password: string;
  sex: string;
  age: number;
  email: string;
  className: string;
  avator: string;
  status: 0 | 1;
  creator: string;
  createTime: 1681971560542,
  role: string;
  classId: string;
  lastOnlineTime: number;
}

export type ClassListItem = {
  _id: string;
  name: string;
  classify: string;
  teacher: string;
  students: Student[];
  creator: string;
  createTime: number;
}

export type ClassListRes = {
  list: ClassListItem[];
  total: number;
  totalPage: number;
}

export type StudentListRes = {
  list: Student[];
  total: number;
  totalPage: number;
}


export type ClassifyItem = {
  _id: string;
  name: string;
  value: string;
}
export type ClassifyRes = {
  list: ClassifyItem[];
  total: number;
  totalPage: number;
}
