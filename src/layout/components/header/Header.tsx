import { Layout, Avatar, Space, Dropdown } from 'antd'
import { UserOutlined, PoweroffOutlined } from '@ant-design/icons'
import style from './header.module.scss'
import { useAppSelector } from '@/hooks/store'
import { useNavigate } from 'react-router-dom'
import type { MenuProps } from 'antd'


const items: MenuProps['items'] = [
  {
    key: 'userInfo',
    label: '个人中心',
    icon: <UserOutlined />
  },
  {
    key: 'logout',
    danger: true,
    label: '退出登录',
    icon: <PoweroffOutlined />
  }
]
const Header = () => {
  const navigate = useNavigate()
  const userInfo = useAppSelector(state => state.user.userInfo)
  const handleClick: MenuProps['onClick'] = item => {
    if (item.key === 'logout') {
      localStorage.removeItem('exam_token')
      navigate('/login')
    }
  }
  return (
    <Layout.Header className={style.header}>
      <div className={style.logo}>在线考试管理系统</div>
      <div className={style.right}>
        <Dropdown
          className={style.dropdown}
          menu={{
            items,
            onClick: handleClick
          }}
        >
          <Space>
            <Avatar className={style.avatar}>
              {userInfo?.username[0]}
            </Avatar>
            {userInfo?.username}
          </Space>
        </Dropdown>
      </div>
    </Layout.Header>
  )
}

export default Header