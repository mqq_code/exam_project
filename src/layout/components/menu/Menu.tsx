import { Layout, Menu } from 'antd'
import { useEffect, useState } from 'react'
import { useAppDispatch } from '@/hooks/store'
import { getMenulist } from '@/store/models/user'
import { useCurrentMenu } from '@/hooks/useCurrentMenu'
import { useLocation } from 'react-router-dom'
import { useLeftMenuList } from '@/hooks/useLeftMenuList'
const { Sider } = Layout

const LayoutMenu = () => {
  const menuList = useLeftMenuList()
  const dispatch = useAppDispatch()
  const location = useLocation()
  // 获取当前正在展示的页面
  const curMenu = useCurrentMenu()
  // 展开的左侧菜单项
  const [openKeys, setOpenKeys] = useState<string[]>([])
  // 设置展开的菜单项
  useEffect(() => {
    setOpenKeys(curMenu.map(v => v.key))
  }, [curMenu])

  useEffect(() => {
    dispatch(getMenulist())
  }, [])

  return (
    <Sider width={200} collapsible theme="light">
      <Menu
        mode="inline"
        defaultSelectedKeys={[location.pathname]}
        openKeys={openKeys}
        style={{
          height: '100%',
          borderRight: 0,
          overflow: 'auto'
        }}
        items={menuList}
        onOpenChange={setOpenKeys}
      />
    </Sider>
  )
}

export default LayoutMenu