import { Outlet } from 'react-router-dom'
import { Layout } from 'antd'
import Header from './components/header/Header'
import LayoutMenu from './components/menu/Menu'
import { getUserInfo } from '@/store/models/user'
import { useAppDispatch, useAppSelector } from '@/hooks/store'
import { useEffect } from 'react'
import PageLoading from '../components/PageLoading/PageLoading'

const LayoutPage = () => {
  const dispatch = useAppDispatch()
  const loading = useAppSelector(state => state.user.loading)

  useEffect(() => {
    // 调用 store 中的方法获取用户信息
    dispatch(getUserInfo())
  }, [])

  if (loading) {
    return <PageLoading />
  }

  return (
    <Layout style={{ height: '100vh', minWidth: 1200 }}>
      <Header />
      <Layout>
        <LayoutMenu />
        <Layout style={{ overflow: 'auto' }}>
          <Outlet />
        </Layout>
      </Layout>
    </Layout>
  )
}
export default LayoutPage