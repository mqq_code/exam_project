import { useMemo } from 'react'
import { useSelector } from 'react-redux'
import { selectMenu } from '@/store/models/user'
import { baseMenu, formatMenu } from '@/utils/menu'

// 获取左侧菜单
export const useLeftMenuList = () => {
  const menuList = useSelector(selectMenu)

  return useMemo(() => {
    return baseMenu.concat(formatMenu(menuList))
  }, [menuList])
}