import { useMemo } from 'react'
import { useLocation } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { selectMenu } from '@/store/models/user'
import { getParentList } from '@/utils/menu'
import { useLeftMenuList } from './useLeftMenuList'

// 获取当前菜单
export const useCurrentMenu = () => {
  const location = useLocation()
  const menuList = useSelector(selectMenu)
  const LeftMenuList = useLeftMenuList()
  return useMemo(() => {
    return getParentList(LeftMenuList, location.pathname) || []
  }, [location.pathname, menuList])
}