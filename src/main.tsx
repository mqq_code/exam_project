import { createRoot } from 'react-dom/client'
import { Suspense } from 'react'
import './index.scss'
import App from './App.jsx'
import { HashRouter } from 'react-router-dom'
import { store } from './store'
import { Provider } from 'react-redux'
import PageLoading from './components/PageLoading/PageLoading'

createRoot(document.getElementById('root')!).render(
  <Suspense fallback={<PageLoading />}>
    <Provider store={store}>
      <HashRouter>
        <App />
      </HashRouter>
    </Provider>
  </Suspense>
)
