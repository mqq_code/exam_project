import { useMemo } from 'react'
import { useAppSelector } from '@/hooks/store'

interface Props {
  permissionKey: string;
  children: React.ReactElement;
}
const Permission: React.FC<Props> = ({
  permissionKey,
  children,
}) => {
  const permission = useAppSelector(state => state.user.userInfo?.permission || [])
  // 去权限列表中查找是否存在此按钮权限
  const showDelBtn = useMemo(() => {
    return permission.find(v => v.path === permissionKey)
  }, [permission, permissionKey])

  if (showDelBtn) {
    return children
  }
  return null
}

export default Permission
