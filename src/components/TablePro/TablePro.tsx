import { useEffect, useState, forwardRef, useImperativeHandle } from 'react'
import { Table, message } from 'antd'
import type { TableProps } from 'antd'

type AnyObject = {
  [x: string]: any;
  [x: number]: any;
  [x: symbol]: any;
}
export interface TableInstance {
  getData: () => void;
}

export type TableRef = React.Ref<TableInstance>


interface PageQuery {
  page: number;
  pagesize: number;
}
type TableProProps<T> =  {
  request: (params: PageQuery) => Promise<{ total?: number; list: T[] }>;
  headerTitle: string;
  toolBarRender?: () => React.ReactNode;
} & TableProps<T>

const TablePro = <RecordType extends AnyObject>({
  request,
  headerTitle,
  toolBarRender,
  ...tableProps
}: TableProProps<RecordType>, ref: TableRef) => {
  const [data, setData] = useState<RecordType[]>([] as RecordType[])
  const [loading, setLoading] = useState(false)
  const [total, setTotal] = useState(0)
  const [query, setQuery] = useState<PageQuery>({ page: 1, pagesize: 5 })

  const getData = async () => {
    try {
      setLoading(true)
      const res = await request(query)
      setLoading(false)
      setTotal(res.total || 0)
      setData(res.list)
    } catch (e) {
      message.error('获取数据失败')
    }
  }

  useEffect(() => {
    getData()
  }, [query])

  // 给 ref 添加数据
  useImperativeHandle(ref, () => {
    // return 的数据会赋值给ref对象的 current 属性
    return {
      getData,
      list: data
    }
  }, [data])

  return (
    <div>
      <div style={{ padding: '16px 0', display: 'flex', justifyContent: 'space-between' }}>
        <h3>{headerTitle}</h3>
        {toolBarRender && toolBarRender()}
      </div>
      <Table
        loading={loading}
        columns={tableProps.columns}
        dataSource={data}
        rowKey={record => record._id}
        pagination={typeof tableProps.pagination === 'boolean' ? tableProps.pagination : {
          total,
          showSizeChanger: true,
          showQuickJumper: true,
          showTotal: t => `共 ${t} 条`,
          current: query.page,
          pageSize: query.pagesize,
          onChange: (page, pagesize) => setQuery({ page, pagesize }),
          ...tableProps.pagination
        }}
        {...tableProps}
      />
    </div>
  )
}


export type RefTable = <RecordType extends AnyObject = AnyObject>(
  props: React.PropsWithChildren<TableProProps<RecordType>> & React.RefAttributes<TableInstance>,
) => React.ReactElement;

// 把父组件传入的 ref 转发给 TablePro 的第二个参数
const TableProRef = forwardRef(TablePro) as unknown as RefTable
export default TableProRef