import { Button, Modal } from 'antd'
import { useState } from 'react'
import type { ButtonProps, ModalProps } from 'antd'


interface Props {
  children: React.ReactNode;
  buttonText: React.ReactNode;
  modalTitle: React.ReactNode;
  cancelText?: React.ReactNode;
  okText?: React.ReactNode;
  buttonProps?: ButtonProps;
  modalProps?: ModalProps;
  onOk: () => Promise<boolean | undefined>;
  onCancel?: () => void;
}

const CreateMenu: React.FC<Props> = ({
  children,
  buttonText,
  modalTitle,
  cancelText = '取消',
  okText = '确定',
  buttonProps = {},
  modalProps = {},
  onOk,
  onCancel
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [confirmLoading, setConfirmLoading] = useState(false)
  const showModal = () => {
    setIsModalOpen(true)
  }
  const handleOk = async () => {
    setConfirmLoading(true)
    const isClose = await onOk()
    setConfirmLoading(false)
    if (isClose !== false) {
      setIsModalOpen(false)
    }
  }
  const handleCancel = () => {
    if (onCancel) {
      onCancel()
    }
    setIsModalOpen(false)
  }
  return (
    <>
      <Button type="primary" onClick={showModal} {...buttonProps}>{buttonText}</Button>
      <Modal
        title={modalTitle}
        confirmLoading={confirmLoading}
        open={isModalOpen}
        cancelText={cancelText}
        okText={okText}
        onOk={handleOk}
        onCancel={handleCancel}
        {...modalProps}
      >
        <div style={{ padding: '20px 0' }}>
          {children}
        </div>
      </Modal>
    </>
  )
}

export default CreateMenu