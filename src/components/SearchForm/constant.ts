export enum FilterType {
  input = 'input',
  select = 'select',
  date = 'date'
}