import { Form, Input, Select, Button, Space, DatePicker } from 'antd'
import style from './searchForm.module.scss'
import classNames from 'classnames'
import type { SelectProps, InputProps  } from 'antd'
import { FilterType } from './constant'

const { RangePicker } = DatePicker

type FilterSelectItem = { type: FilterType.select, props?: SelectProps }
type FilterInputItem = { type: FilterType.input, props?: InputProps }
type FilterDateItem = { type: FilterType.date, props?: any }
type FilterItem = FilterSelectItem | FilterInputItem | FilterDateItem

export type FilterListItem = {
  label: string
  name: string
  placeholder: string
} & FilterItem

type NameKeyedObject<T extends { name: string }[]> = {
  [K in T[number]['name']]: any;
}

interface Props{
  filterList: FilterListItem[]
  onSearch: (params: NameKeyedObject<FilterListItem[]>) => void
  onReset: (params: NameKeyedObject<FilterListItem[]>) => void
}

const SearchForm = ({
  filterList,
  onSearch,
  onReset
}: Props) => {
  // form 组件实例
  const [form] = Form.useForm()

  const onFinish = (params: NameKeyedObject<typeof filterList>) => {
    const res: NameKeyedObject<typeof filterList> = {}
    Object.entries(params).forEach(([key, val]) => {
      if (val !== undefined && val !== '') {
        res[key] = val
      }
    })
    onSearch(res)
  }
  const renderItem = (item: FilterListItem) => {
    if (item.type === FilterType.select) {
      return <Select placeholder={item.placeholder} options={item.props?.options || []} {...item.props} /> 
    } else if (item.type === FilterType.date) {
      return <RangePicker {...item.props} />
    } else {
      return <Input placeholder={item.placeholder} /> 
    }
  }
  return (
    <Form
      form={form}
      labelCol={{
        span: 6,
      }}
      wrapperCol={{
        span: 20,
      }}
      className={style.form}
      onFinish={onFinish}
    >
      {filterList.map(item =>
        <Form.Item className={style.formItem} key={item.label} label={item.label} name={item.name}>
          {renderItem(item)}
        </Form.Item>
      )}
      <Form.Item className={classNames(style.formItem, style.btns)} wrapperCol={{ span: 24 }}>
        <Space>
          <Button type="primary" onClick={form.submit}>搜索</Button>
          <Button onClick={() => {
            form.resetFields()
            onReset({})
          }}>重置</Button>
        </Space>
      </Form.Item>
    </Form>
  )
}

export default SearchForm