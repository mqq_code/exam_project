import style from './pageHeader.module.scss'
import { Breadcrumb } from 'antd'
import { useCurrentMenu } from '@/hooks/useCurrentMenu'

const PageHeader: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const curMenu = useCurrentMenu()
  return (
    <div>
      <div className={style.pageHeader}>
        <Breadcrumb items={curMenu} />
        <h3>{curMenu[curMenu.length - 1]?.title}</h3>
      </div>
      {children}
    </div>
  )
}

export default PageHeader