import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'
// 安装运行服务时执行 eslint 插件
import eslintPlugin from 'vite-plugin-eslint'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    // 运行时执行eslint
    eslintPlugin({
      include: ['src/**/*.ts', 'src/**/*.tsx']
    })
  ],
  resolve: {
    alias: {
      '@': path.resolve('src')
    }
  },
  server: {
    port: 8000,
    proxy: {
      '/api': {
        target: 'http://localhost:3001',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      }
    },
  }
})
